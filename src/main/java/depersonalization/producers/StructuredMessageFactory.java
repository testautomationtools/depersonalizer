package depersonalization.producers;

public interface StructuredMessageFactory {

    StructuredMessage nextMessage();

    boolean hasMoreMessages();

}
