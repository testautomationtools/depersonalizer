package depersonalization.producers;

public class XMLMessage implements StructuredMessage {

    private static final String TYPE = "XML";
    private final String content;

    public XMLMessage(java.lang.String content) {
        this.content = content;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public String getContent() {
        return content;
    }
}
