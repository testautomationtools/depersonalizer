package depersonalization.producers;

import depersonalization.producers.resources.Resource;

public class XMLMessageFactory implements StructuredMessageFactory {

    private Resource resource;

    public static XMLMessageFactoryBuilder newInstance() {
        return new XMLMessageFactoryBuilder();
    }

    private XMLMessageFactory(XMLMessageFactoryBuilder builder) {
        this.resource = builder.resource;
    }

    @Override
    public StructuredMessage nextMessage() {
        java.lang.String content = resource.next();
        return new XMLMessage(content);
    }

    @Override
    public boolean hasMoreMessages() {
        return resource.hasNext();
    }

    public static class XMLMessageFactoryBuilder {

        private Resource resource;

        public XMLMessageFactoryBuilder setResource(Resource resource) {
            this.resource = resource;
            return this;
        }

        public XMLMessageFactory build() {
            return new XMLMessageFactory(this);
        }
    }
}
