package depersonalization.producers.resources;

public interface Resource {

    String next();

    boolean hasNext();

}
