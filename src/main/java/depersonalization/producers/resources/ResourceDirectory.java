package depersonalization.producers.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static depersonalization.utils.IOUtils.directoryCheck;
import static depersonalization.utils.IOUtils.fileContent;

public class ResourceDirectory implements Resource {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceDirectory.class);
    private Iterator<File> fileIterator;

    public ResourceDirectory(File directory) {
        directoryCheck(directory);
        List<File> files = getFilesFromDirectory(directory);
        fileIterator = files.iterator();
    }

    @Override
    public String next() {
        File file = fileIterator.next();
        return fileContent(file);
    }

    @Override
    public boolean hasNext() {
        return fileIterator.hasNext();
    }

    private List<File> getFilesFromDirectory(File directory) {
        File[] allFiles = directory.listFiles();
        if (allFiles == null) {
            LOG.debug("listFiles() returned null from checked directory [{}]. This should not happen!!!", directory);
            return Collections.emptyList();
        } else {
            return getOnlyNormalFiles(allFiles);
        }
    }

    private List<File> getOnlyNormalFiles(File[] allFiles) {
        List<File> normalFiles = new ArrayList<>();
        for (File file : allFiles) {
            if (file.isFile()) {
                normalFiles.add(file);
            } else {
                LOG.debug("attempt to add file [{}] that is not a normal file", file.getAbsoluteFile());
            }
        }
        return normalFiles;
    }
}
