package depersonalization.producers;

public interface StructuredMessage {

    String getType();

    String getContent();

}
