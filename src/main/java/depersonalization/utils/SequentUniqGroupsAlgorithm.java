package depersonalization.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class SequentUniqGroupsAlgorithm<E> {

    private final List<E> list;

    public SequentUniqGroupsAlgorithm(final List<E> list) {
        Objects.requireNonNull(list, "List of elements must not be null");
        if (list.isEmpty()) {
            throw new IllegalArgumentException("Can't find elements in empty List");
        }
        this.list = list;
    }

    public List<List<E>> find(int groupSize) {
        if (groupSize <= 0) {
            throw new IllegalArgumentException("Negative numbers and zeros are not allowed for group size");
        }
        if (list.size() <= groupSize) {
            return Collections.singletonList(list);
        }
        List<List<E>> allGroups = new ArrayList<>();
        for (int indentation = 0; indentation < groupSize; indentation++) {
            List<List<E>> sameIndentationGroups = new ArrayList<>();
            for (int i = indentation; list.size() - i >= groupSize; i += groupSize) {
                List<E> singleGroup = new ArrayList<>();
                for (int j = 0; j < groupSize; j++) {
                    singleGroup.add(list.get(i + j));
                }
                sameIndentationGroups.add(singleGroup);
            }
            allGroups.addAll(sameIndentationGroups);
        }
        return allGroups;
    }

}
