package depersonalization.utils;

import java.util.Arrays;
import java.util.List;

public class Common {
    private Common() {
    }

    public static <T> List<T> arrayToList(T[] array) {
        return Arrays.asList(array);
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }
}
