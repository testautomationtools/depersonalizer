package depersonalization.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class IOUtils {

    private static final Logger LOG = LoggerFactory.getLogger(IOUtils.class);

    private IOUtils() {

    }

    public static List<String> getNotEmptyLinesFromResource(String resourceName) {
        List<String> allLines = getAllLinesFromResource(resourceName);
        return allLines.stream()
                .filter(x -> !x.isEmpty())
                .collect(Collectors.toList());
    }

    public static List<String> getAllLinesFromResource(String resourceName) {
        File file = fileFromResource(resourceName);
        return getLinesFromFile(file);
    }

    public static List<String> getLinesFromFile(File file) {
        fileCheck(file);
        List<String> lines = new ArrayList<>();
        try {
            lines.addAll(Files.readAllLines(file.toPath()));
        } catch (IOException e) {
            LOG.error("Unable to read all lines from file [{}]", file.getAbsoluteFile(), e);
        }
        return lines;
    }

    public static File fileFromResource(String resourceName) {
        if (Common.isNullOrEmpty(resourceName)) {
            throw new IllegalArgumentException("resourceName must not be null or empty");
        }
        URL resourceURL = IOUtils.class.getClassLoader().getResource(resourceName);
        if (resourceURL == null) {
            throw new IllegalArgumentException(String.format("resource with name [%s] does not exist", resourceName));
        }
        return new File(resourceURL.getFile());
    }

    public static void fileCheck(File file) {
        Objects.requireNonNull(file, "file must not be null");
        if (!file.exists()) {
            LOG.error("An attempt was made to check file [{}] that does not exist", file.getAbsoluteFile());
            throw new IllegalArgumentException(String.format("file [%s] not exists", file));
        }
    }

    public static void directoryCheck(File file) {
        fileCheck(file);
        if (!file.isDirectory()) {
            throw new IllegalArgumentException(String.format("file [%s] is not a directory", file));
        }
    }

    public static String fileContent(File file) {
        List<String> lines = getLinesFromFile(file);
        StringBuilder stringBuilder = new StringBuilder();
        for (String line : lines) {
            stringBuilder.append(line).append("\n");
        }
        return stringBuilder.toString();
    }
}
