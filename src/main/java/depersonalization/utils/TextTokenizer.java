package depersonalization.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

public class TextTokenizer {
    private final String text;
    private List<String> words;
    private boolean found;
    private SequentUniqGroupsAlgorithm<String> groupFinder;

    public TextTokenizer(String text) {
        Objects.requireNonNull(text, "Text for tokenizer should not be null");
        this.text = text;
    }

    public List<String> getWords() {
        if (!found) {
            findWordsInText();
        }
        return words;
    }

    public List<List<String>> getSequentGroupsOfSize(int size) {
        if (groupFinder == null) {
            groupFinder = new SequentUniqGroupsAlgorithm<>(getWords());
        }
        return groupFinder.find(size);
    }

    private void findWordsInText() {
        String inputWithExtraSpaces = addWhiteSpaceAfterDot(text);
        StringTokenizer tokenizer = new StringTokenizer(inputWithExtraSpaces);
        words = tokenizerToStringList(tokenizer);
        found = true;
    }

    private String addWhiteSpaceAfterDot(String input) {
        char[] inputChars = input.toCharArray();
        List<Character> withWhiteSpaces = new ArrayList<>();
        for (char inputChar : inputChars) {
            withWhiteSpaces.add(inputChar);
            if (inputChar == '.') {
                withWhiteSpaces.add(' ');
            }
        }
        return charListToString(withWhiteSpaces);
    }

    private String charListToString(List<Character> charsList) {
        char[] chars = new char[charsList.size()];
        for (int i = 0; i < charsList.size(); i++) {
            chars[i] = charsList.get(i);
        }
        return new String(chars);
    }

    private List<String> tokenizerToStringList(StringTokenizer tokenizer) {
        List<String> tokens = new ArrayList<>();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            tokens.add(token);
        }
        return tokens;
    }
}
