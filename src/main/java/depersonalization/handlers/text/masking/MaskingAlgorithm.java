package depersonalization.handlers.text.masking;

import java.util.function.UnaryOperator;

public interface MaskingAlgorithm extends UnaryOperator<String> {
}
