package depersonalization.handlers.text.masking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static depersonalization.utils.Common.isNullOrEmpty;

public class BasicMaskingAlgorithm implements MaskingAlgorithm {

    private List<CharMasker> charactersGroups;

    public static BasicMaskingAlgorithmBuilder newInstance() {
        return new BasicMaskingAlgorithmBuilder();
    }

    private BasicMaskingAlgorithm(BasicMaskingAlgorithmBuilder builder) {
        charactersGroups = Collections.unmodifiableList(builder.characterGroups);
    }

    @Override
    public String apply(String input) {
        char[] original = input.toCharArray();
        char[] masked = new char[original.length];
        for (int i = 0; i < original.length; i++) {
            char newChar = getMaskForChar(original[i]);
            masked[i] = newChar;
        }
        return new String(masked);
    }

    private char getMaskForChar(char ch) {
        for (CharMasker masker : charactersGroups) {
            int founded = Arrays.binarySearch(masker.dictionary, ch);
            if (founded >= 0) {
                return masker.mask;
            }
        }
        return ch;
    }

    public static class BasicMaskingAlgorithmBuilder {

        private List<CharMasker> characterGroups;

        private BasicMaskingAlgorithmBuilder() {
            this.characterGroups = new ArrayList<>();
        }

        public BasicMaskingAlgorithmBuilder addMaskForCharacters(String characterGroup, char mask) {
            char[] chars = getSortedCharArrayFrom(characterGroup);
            characterGroups.add(new CharMasker(chars, mask));
            return this;
        }

        public BasicMaskingAlgorithmBuilder addMaskForCharacters(String characterGroup) {
            char[] chars = getSortedCharArrayFrom(characterGroup);
            characterGroups.add(new CharMasker(chars, chars[0]));
            return this;
        }

        public BasicMaskingAlgorithm build() {
            if (characterGroups.isEmpty()) {
                throw new IllegalStateException("At least one character group and mask for it should be presented");
            }
            return new BasicMaskingAlgorithm(this);
        }

        private char[] getSortedCharArrayFrom(String characterGroup) {
            if (isNullOrEmpty(characterGroup)) {
                throw new IllegalArgumentException("characterGroup should not be null or empty");
            }
            char[] chars = characterGroup.toCharArray();
            Arrays.sort(chars);
            return chars;
        }
    }

    static class CharMasker {
        private char[] dictionary;
        private char mask;

        private CharMasker(char[] dictionary, char mask) {
            this.dictionary = dictionary;
            this.mask = mask;
        }
    }

    List<CharMasker> getCharactersGroups() {
        return charactersGroups;
    }
}
