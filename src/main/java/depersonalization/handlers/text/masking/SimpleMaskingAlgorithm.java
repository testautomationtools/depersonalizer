package depersonalization.handlers.text.masking;

public class SimpleMaskingAlgorithm implements MaskingAlgorithm {

    private final char mask;

    public SimpleMaskingAlgorithm(char mask) {
        this.mask = mask;
    }

    @Override
    public String apply(String input) {
        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            stringBuffer.append(mask);
        }
        return stringBuffer.toString();
    }
}
