package depersonalization.handlers.text.node;

import depersonalization.handlers.text.node.rules.TextNodeRule;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class TextNodeProcessor {

    private List<TextNodeRule> rules;
    private UnaryOperator<String> maskingAlgorithm;

    public static TextNodeDePersonProcessorBuilder newInstance() {
        return new TextNodeDePersonProcessorBuilder();
    }

    public TextNodeProcessor(TextNodeDePersonProcessorBuilder builder) {
        rules = builder.rules;
        maskingAlgorithm = builder.maskingAlgorithm;
    }

    public String depersonalize(String nodeName, String nodeValue) {
        for (TextNodeRule rule : rules) {
            boolean accepted = rule.test(nodeName, nodeValue);
            if (accepted) {
                return maskingAlgorithm.apply(nodeValue);
            }
        }
        return nodeValue;
    }

    public static class TextNodeDePersonProcessorBuilder {
        private List<TextNodeRule> rules;
        private UnaryOperator<String> maskingAlgorithm;

        public TextNodeDePersonProcessorBuilder() {
            rules = new ArrayList<>();
        }

        public TextNodeDePersonProcessorBuilder addRule(TextNodeRule rule) {
            rules.add(rule);
            return this;
        }

        public TextNodeDePersonProcessorBuilder setMaskingAlgorithm(UnaryOperator<String> maskingAlgorithm) {
            this.maskingAlgorithm = maskingAlgorithm;
            return this;
        }

        public TextNodeProcessor build() {
            return new TextNodeProcessor(this);
        }
    }
}
