package depersonalization.handlers.text.node.rules.finders;

import java.util.function.Predicate;

public interface TextSequenceFinder extends Predicate<String> {

}
