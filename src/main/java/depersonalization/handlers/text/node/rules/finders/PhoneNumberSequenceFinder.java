package depersonalization.handlers.text.node.rules.finders;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//TODO Using regular expressions is security-sensitive
// need to find another option
public class PhoneNumberSequenceFinder implements TextSequenceFinder {

    private static final String PHONE_PATTERN = "((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}";
    private final Pattern phonePattern;

    public PhoneNumberSequenceFinder() {
        phonePattern = Pattern.compile(PHONE_PATTERN);
    }

    @Override
    public boolean test(String text) {
        Matcher phoneMatcher = phonePattern.matcher(text);
        return phoneMatcher.find();
    }
}
