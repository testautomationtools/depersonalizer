package depersonalization.handlers.text.node.rules.finders;

import depersonalization.utils.TextTokenizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonNameSequenceFinder implements TextSequenceFinder {

    @Override
    public boolean test(String text) {
        List<List<String>> groups = extractGroupsOfTwoAndTreeWordsFromText(text);
        for (List<String> groupOfWords : groups) {
            if (wordsContainsPersonalData(groupOfWords)) {
                return true;
            }
        }
        return false;
    }

    private List<List<String>> extractGroupsOfTwoAndTreeWordsFromText(String text) {
        TextTokenizer textTokenizer = new TextTokenizer(text);
        List<String> wordsFromText = textTokenizer.getWords();
        boolean textContainsOneWord = wordsFromText.size() == 1;
        if (wordsFromText.isEmpty() || textContainsOneWord) {
            return Collections.emptyList();
        }
        List<List<String>> groups = new ArrayList<>();
        groups.addAll(textTokenizer.getSequentGroupsOfSize(2));
        groups.addAll(textTokenizer.getSequentGroupsOfSize(3));
        return groups;
    }

    private boolean wordsContainsPersonalData(List<String> groupOfWords) {
        return everyWordStartsWithUpperCase(groupOfWords)
                && atLeastOneWordHasMoreThenOneChar(groupOfWords)
                && ifLengthEqualsOneThenNextCharIsDot(groupOfWords);
    }

    private boolean everyWordStartsWithUpperCase(List<String> words) {
        for (String word : words) {
            char firstChar = word.charAt(0);
            boolean firstCharIsUppercase = Character.isUpperCase(firstChar);
            if (!firstCharIsUppercase) {
                return false;
            }
        }
        return true;
    }

    private boolean atLeastOneWordHasMoreThenOneChar(List<String> words) {
        for (String word : words) {
            if (word.length() > 1) {
                return true;
            }
        }
        return false;
    }

    private boolean ifLengthEqualsOneThenNextCharIsDot(List<String> words) {
        List<String> twoCharGroups = new ArrayList<>();
        for (String word : words) {
            if (word.length() == 2) {
                twoCharGroups.add(word);
            }
        }
        return startsWithUppercaseThenNextCharIsDot(twoCharGroups);
    }

    private boolean startsWithUppercaseThenNextCharIsDot(List<String> twoCharGroups) {
        for (String twoChars : twoCharGroups) {
            char first = twoChars.charAt(0);
            char second = twoChars.charAt(1);
            boolean accepted = Character.isUpperCase(first) && second == '.';
            if (!accepted) {
                return false;
            }
        }
        return true;
    }
}
