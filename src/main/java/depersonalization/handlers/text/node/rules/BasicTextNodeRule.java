package depersonalization.handlers.text.node.rules;

import depersonalization.handlers.text.node.rules.finders.TextSequenceFinder;

import java.util.Collections;
import java.util.List;

public class BasicTextNodeRule implements TextNodeRule {

    private final List<TextSequenceFinder> finders;

    public BasicTextNodeRule(final List<TextSequenceFinder> finders) {
        if (finders.isEmpty()) {
            throw new IllegalArgumentException("list of finders should not be empty");
        }
        this.finders = Collections.unmodifiableList(finders);
    }

    @Override
    public boolean test(String nodeName, String nodeValue) {
        for (TextSequenceFinder finder : finders) {
            boolean founded = finder.test(nodeValue);
            if (founded) {
                return true;
            }
        }
        return false;
    }
}
