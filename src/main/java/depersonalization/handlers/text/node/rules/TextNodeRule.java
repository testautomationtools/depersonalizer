package depersonalization.handlers.text.node.rules;

import java.util.function.BiPredicate;

public interface TextNodeRule extends BiPredicate<String, String> {
}
