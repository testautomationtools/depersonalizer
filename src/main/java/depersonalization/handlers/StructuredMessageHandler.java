package depersonalization.handlers;

import depersonalization.producers.StructuredMessage;

import java.util.Optional;

public interface StructuredMessageHandler {

    String getType();

    Optional<String> dePerson(StructuredMessage message);

}
