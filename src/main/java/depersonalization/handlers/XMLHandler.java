package depersonalization.handlers;

import de.pdark.decentxml.*;
import depersonalization.handlers.text.node.TextNodeProcessor;
import depersonalization.producers.StructuredMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

public class XMLHandler implements StructuredMessageHandler {

    private static final Logger LOG = LoggerFactory.getLogger(XMLHandler.class);
    private static final String TYPE = "XML";
    private XMLParser parser;
    private TextNodeProcessor depersonalizer;

    public XMLHandler(TextNodeProcessor depersonalizer) {
        this.depersonalizer = depersonalizer;
        parser = new XMLParser();
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public Optional<String> dePerson(StructuredMessage message) {
        try {
            Document document = parser.parse(new XMLStringSource(message.getContent()));
            TreeIterator iterator = document.iterator();
            iterateNodes(iterator);
            return Optional.of(document.toXML());
        } catch (XMLParseException ex) {
            LOG.error("");
            return Optional.empty();
        }
    }

    private void iterateNodes(TreeIterator iterator) {
        while (iterator.hasNext()) {
            Node node = iterator.next();
            if (node.getType().equals(XMLTokenizer.Type.ELEMENT)) {
                Element element = (Element) node;
                if (!element.hasChildren()) {
                    String text = element.getText();
                    String dePersonalizedContent = depersonalizer.depersonalize(element.getName(), text);
                    element.setText(dePersonalizedContent);
                }
                List<Attribute> attributes = element.getAttributes();
                for (Attribute attribute : attributes) {
                    String text = attribute.getValue();
                    String dePersonalizedContent = depersonalizer.depersonalize(attribute.getName(), text);
                    attribute.setValue(dePersonalizedContent);
                }
            }
        }
    }
}
