package depersonalization;

import depersonalization.handlers.StructuredMessageHandler;
import depersonalization.producers.StructuredMessage;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DePerson {

    private Map<String, StructuredMessageHandler> handlers;

    private DePerson(DePersonBuilder builder) {
        handlers = builder.handlers;
    }

    public static DePersonBuilder newInstance() {
        return new DePersonBuilder();
    }

    public Optional<String> dePerson(StructuredMessage message) {
        String type = message.getType();
        if (handlers.containsKey(type)) {
            return handlers.get(type).dePerson(message);
        } else {
            return Optional.empty();
        }
    }

    public static class DePersonBuilder {
        private Map<String, StructuredMessageHandler> handlers;

        private DePersonBuilder() {
            this.handlers = new HashMap<>();
        }

        public DePersonBuilder registerHandler(StructuredMessageHandler handler) {
            this.handlers.put(handler.getType(), handler);
            return this;
        }

        public DePerson build() {
            return new DePerson(this);
        }
    }
}
