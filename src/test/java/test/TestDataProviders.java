package test;

import depersonalization.utils.IOUtils;
import org.junit.jupiter.params.provider.Arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class TestDataProviders {

    private TestDataProviders() {

    }

    private static final String NAME_SURNAME_COMBINATIONS = "dictionaries/examples/NameSurnameLastnameCombinations.txt";
    private static final String SURNAME_INITIALS_COMBINATIONS = "dictionaries/examples/SurnameAndInitialsCombinations.txt";

    public static Stream<Arguments> provideNamesCombinations() {
        List<String> lines = IOUtils.getNotEmptyLinesFromResource(NAME_SURNAME_COMBINATIONS);
        return lines.stream().map(Arguments::of);
    }

    public static Stream<Arguments> provideNamesWithInitialsCombinations() {
        List<String> lines = IOUtils.getNotEmptyLinesFromResource(SURNAME_INITIALS_COMBINATIONS);
        return lines.stream().map(Arguments::of);
    }

    public static Stream<Arguments> provideTextWithNamesCombinations() {
        List<String> lines = IOUtils.getNotEmptyLinesFromResource(NAME_SURNAME_COMBINATIONS);
        List<String> sentences = provideSentencesWithStringSequences(lines);
        return sentences.stream().map(Arguments::of);
    }

    public static Stream<Arguments> provideTextWithNamesAndInitialsCombinations() {
        List<String> lines = IOUtils.getNotEmptyLinesFromResource(SURNAME_INITIALS_COMBINATIONS);
        List<String> sentences = provideSentencesWithStringSequences(lines);
        return sentences.stream().map(Arguments::of);
    }

    private static List<String> provideSentencesWithStringSequences(List<String> sequences) {
        List<String> sentences = new ArrayList<>();
        for (String nameSurname : sequences) {
            sentences.add("Текст содержит ФИО: " + nameSurname + " - конец предложения");
        }
        return sentences;
    }
}
