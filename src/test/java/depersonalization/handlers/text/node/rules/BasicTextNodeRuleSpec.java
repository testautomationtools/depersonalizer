package depersonalization.handlers.text.node.rules;

import depersonalization.handlers.text.node.rules.finders.TextSequenceFinder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("BasicTextNodeRuleSpec")
class BasicTextNodeRuleSpec {

    @DisplayName("BasicTextNodeRule с указанием одного TextSequenceFinder")
    @Nested
    class WithSingleTextSequenceFinder {

        @DisplayName("Входная строка соответствует условиям прописаным в TextSequenceFinder")
        @Test
        void inputAccepted() {
            List<TextSequenceFinder> list = Collections.singletonList(s -> s.contains("F"));
            BasicTextNodeRule basicRule = new BasicTextNodeRule(list);
            boolean accepted = basicRule.test("", "Some text that contains F");
            assertTrue(accepted);
        }

        @DisplayName("Входная строка НЕ соответствует условиям прописаным в TextSequenceFinder")
        @Test
        void inputNotAccepted() {
            List<TextSequenceFinder> list = Collections.singletonList(s -> !s.contains("F"));
            BasicTextNodeRule basicRule = new BasicTextNodeRule(list);
            boolean accepted = basicRule.test("", "Some text that contains F");
            assertFalse(accepted);
        }

        @DisplayName("BasicTextNodeRule не реагирует на nodeName, а только nodeValue. Когда входная строка удовлетворяет TextSequenceFinder")
        @ParameterizedTest
        @ValueSource(strings = {"", "F", "TEST", "-1"})
        void inputAccepted(String nodeName) {
            List<TextSequenceFinder> list = Collections.singletonList(s -> s.contains("F"));
            BasicTextNodeRule basicRule = new BasicTextNodeRule(list);
            boolean accepted = basicRule.test(nodeName, "Some text that contains F");
            assertTrue(accepted);
        }

        @DisplayName("BasicTextNodeRule не реагирует на nodeName, а только nodeValue. Когда входная строка НЕ удовлетворяет TextSequenceFinder")
        @ParameterizedTest
        @ValueSource(strings = {"", "F", "TEST", "-1"})
        void inputNotAccepted(String nodeName) {
            List<TextSequenceFinder> list = Collections.singletonList(s -> !s.contains("F"));
            BasicTextNodeRule basicRule = new BasicTextNodeRule(list);
            boolean accepted = basicRule.test(nodeName, "Some text that contains F");
            assertFalse(accepted);
        }
    }

    @DisplayName("BasicTextNodeRule инициализирован с множеством TextSequenceFinder")
    @Nested
    class WithMultipleTextSequenceFinder {

        @DisplayName("Входная строка соответствует условиям прописаным хотя бы в одном из TextSequenceFinder")
        @Test
        void inputAccepted() {
            List<TextSequenceFinder> list = new ArrayList<>();
            list.add(input -> input.contains("ORAORAORAORA"));
            list.add(input -> input.contains("SAWARDO"));
            list.add(input -> input.contains("F"));
            BasicTextNodeRule basicRule = new BasicTextNodeRule(list);
            boolean accepted = basicRule.test("", "Some text that contains F");
            assertTrue(accepted);
        }

        @DisplayName("Входная строка НЕ соответствует ни одному условию списка TextSequenceFinder")
        @Test
        void inputNotAccepted() {
            List<TextSequenceFinder> list = new ArrayList<>();
            list.add(input -> input.contains("ORAORAORAORA"));
            list.add(input -> input.contains("SAWARDO"));
            list.add(input -> input.contains("JOJO"));
            BasicTextNodeRule basicRule = new BasicTextNodeRule(list);
            boolean accepted = basicRule.test("", "Some text that contains F");
            assertFalse(accepted);
        }
    }

    @DisplayName("Конструирование объекта")
    @Nested
    class BasicTextNodeRuleConstructing {

        @DisplayName("Успешное конструирование. Список с элементами имплементирующими TextSequenceFinder")
        @Test
        void normalPath() {
            List<TextSequenceFinder> finders = new ArrayList<>();
            finders.add(input -> true);
            assertDoesNotThrow(() -> new BasicTextNodeRule(finders));
        }

        @DisplayName("Попытка передать null в качестве аргумента для BasicTextNodeRule")
        @Test
        void attemptToPassNullArgument() {
            assertThrows(NullPointerException.class, () -> new BasicTextNodeRule(null));
        }

        @DisplayName("Попытка передать пустой список в качестве аргумента для BasicTextNodeRule")
        @Test
        void attemptToPassEmptyListAsArgument() {
            assertThrows(IllegalArgumentException.class, () -> new BasicTextNodeRule(new ArrayList<>()));
        }
    }
}
