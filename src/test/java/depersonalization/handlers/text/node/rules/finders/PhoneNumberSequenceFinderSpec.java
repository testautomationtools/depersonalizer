package depersonalization.handlers.text.node.rules.finders;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class PhoneNumberSequenceFinderSpec {

    private static TextSequenceFinder phoneNumberFinder;
    private static final String STRING_CONTAINS_PHONE = "Строка [%s] содержит номер телефона";

    @BeforeAll
    static void init() {
        phoneNumberFinder = new PhoneNumberSequenceFinder();
    }

    @ParameterizedTest(name = "Только номер телефона [{0}]")
    @ValueSource(strings = {"+79261234567"
            , "89261234567"
            , "79261234567"
            , "+7 926 123 45 67"
            , "8(926)123-45-67"
            , "123-45-67"
            , "9261234567"
            , "79261234567"
            , "(495)1234567"
            , "(495) 123 45 67"
            , "89261234567"
            , "8-926-123-45-67"
            , "8 927 1234 234"
            , "8 927 12 12 888"
            , "8 927 12 555 12"
            , "8 927 123 8 123"})
    void inputTextIsPhoneNumber(String textWithPhoneNumber) {
        boolean found = phoneNumberFinder.test(textWithPhoneNumber);
        Assertions.assertTrue(found, String.format("Не удалось распознать строку [%s], как номер телефона", textWithPhoneNumber));
    }

    @ParameterizedTest(name = "Номер телефона в тексте [{0}]")
    @ValueSource(strings = {"Текст до номера +79261234567 текст после"
            , "Текст до номера+89261234567-текст после"
            , "Текст до номера 79261234567 текст после"
            , "Текст до номера +7 926 123 45 67 текст после"
            , "Текст до номера 8(926)123-45-67 текст после"
            , "Текст до номера 123-45-67 текст после"
            , "Текст до номера 9261234567 текст после"
            , "Текст до номера 79261234567 текст после"
            , "Текст до номера (495)1234567 текст после"
            , "Текст до номера (495) 123 45 67 текст после"
            , "Текст до номера9892612345670текст после"
            , "Текст до номера 8-926-123-45-67 текст после"
            , "Текст до номера 8 927 1234 234 текст после"
            , "Текст до номера 8 927 12 12 888 текст после"
            , "Текст до номера 8 927 12 555 12 текст после"
            , "Текст до номера 8 927 123 8 123 текст после"})
    void inputTextContainsPhoneNumber(String textWithPhoneNumber) {
        boolean found = phoneNumberFinder.test(textWithPhoneNumber);
        Assertions.assertTrue(found, String.format("Не удалось найти в строке [%s] номер телефона", textWithPhoneNumber));
    }

    @Test
    void inputTextContainsOneDigit() {
        String containsOneDigit = "5 дней";
        boolean found = phoneNumberFinder.test(containsOneDigit);
        Assertions.assertFalse(found, String.format(STRING_CONTAINS_PHONE, containsOneDigit));
    }

    @Test
    void inputTextContainsTwoDigits() {
        String containsTwoDigits = "Code is 05";
        boolean found = phoneNumberFinder.test(containsTwoDigits);
        Assertions.assertFalse(found, String.format(STRING_CONTAINS_PHONE, containsTwoDigits));
    }

    @Test
    void inputTextContainsThreeDigits() {
        String containsThreeDigits = "SRC-101";
        boolean found = phoneNumberFinder.test(containsThreeDigits);
        Assertions.assertFalse(found, String.format(STRING_CONTAINS_PHONE, containsThreeDigits));
    }

}
