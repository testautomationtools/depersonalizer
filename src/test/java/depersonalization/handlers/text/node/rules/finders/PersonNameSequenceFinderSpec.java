package depersonalization.handlers.text.node.rules.finders;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class PersonNameSequenceFinderSpec {

    private static TextSequenceFinder nameSurnameFinder;
    private static final String DOES_NOT_CONTAIN_NAME = "Строка [%s] не содержит фамилию, имя или отчество";
    private static final String DOES_CONTAIN_NAME = "Строка [%s] содержит фамилию, имя или отчество";

    @BeforeAll
    static void init() {
        nameSurnameFinder = new PersonNameSequenceFinder();
    }

    @Disabled("Не участвует в регрессе")
    @DisplayName("Для подбора тестовых случев")
    @Nested
    class FastSingleTest {

        @DisplayName("Быстро проверить строку на наличие ФИО")
        @Test
        void selfTestCheckOneText() {
            stringContainsName("Деревнин М. А");
        }

        @DisplayName("Быстро проверить строку на отсутствие ФИО")
        @Test
        void doesNotContainNames() {
            stringDoesNotContainName("ORA-101");
        }
    }

    @DisplayName("Проверка пустых строк и строк из одного слова")
    @Nested
    class InputStructuredMessageEmptyOrOneWord {

        @DisplayName("Проверка пустой строки")
        @Test
        void emptyStringDoesNotContainName() {
            stringDoesNotContainName("");
        }

        @DisplayName("Проверка пустой строки")
        @Test
        void oneWordStringDoesNotContainName() {
            stringDoesNotContainName("Вентура");
        }
    }

    @DisplayName("Проверка строк из 2 или 3 слов")
    @Nested
    class InputStructuredMessageThreeOrTwoWords {

        @DisplayName("Проверка на ФИО должна срабатывать для различных комбинаций фамилий, имени и отчества")
        @ParameterizedTest
        @MethodSource("test.TestDataProviders#provideNamesCombinations")
        void nameSurnameCombinations(String textWithName) {
            stringContainsName(textWithName);
        }

        @DisplayName("Проверка на ФИО должна срабатывать для различных комбинаций фамилий и инициалов")
        @ParameterizedTest
        @MethodSource("test.TestDataProviders#provideNamesWithInitialsCombinations")
        void surnameAndInitialsCombinations(String textWithName) {
            stringContainsName(textWithName);
        }
    }

    @DisplayName("Проверка строк в которых больше 3 слов")
    @Nested
    class InputStructuredMessageContainsMoreThenThreeWords {

        @DisplayName("Проверка на ФИО должна срабатывать для различных комбинаций фамилий, имени и отчества")
        @ParameterizedTest
        @MethodSource("test.TestDataProviders#provideTextWithNamesCombinations")
        void nameSurnameCombinations(String textWithName) {
            stringContainsName(textWithName);
        }

        @DisplayName("Проверка на ФИО должна срабатывать для различных комбинаций фамилий и инициалов")
        @ParameterizedTest
        @MethodSource("test.TestDataProviders#provideTextWithNamesAndInitialsCombinations")
        void surnameAndInitialsCombinations(String textWithName) {
            stringContainsName(textWithName);
        }
    }

    @DisplayName("Строки, в которых не содержится частей ФИО")
    @Nested
    class StringsWithoutFIO {

        @Test
        void test1() {
            stringDoesNotContainName("Получена ошибка");
        }

        @Test
        void test2() {
            stringDoesNotContainName("101");
        }

        @Test
        void test3() {
            stringDoesNotContainName("ORA-90 - Hado number ninety");
        }

        @Test
        void test4() {
            stringDoesNotContainName("ORA-90 hado number ninety");
        }

        @Test
        void test5() {
            stringDoesNotContainName("Glk");
        }
    }

    private static void stringContainsName(String input) {
        boolean found = nameSurnameFinder.test(input);
        Assertions.assertTrue(found, String.format(DOES_NOT_CONTAIN_NAME, input));
    }

    private static void stringDoesNotContainName(String input) {
        boolean found = nameSurnameFinder.test(input);
        Assertions.assertFalse(found, String.format(DOES_CONTAIN_NAME, input));
    }
}
