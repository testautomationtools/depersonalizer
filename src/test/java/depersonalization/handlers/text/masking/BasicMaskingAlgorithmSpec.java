package depersonalization.handlers.text.masking;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("BasicMaskingAlgorithm")
class BasicMaskingAlgorithmSpec {

    @DisplayName("Проверка маскирования строки")
    @Nested
    class MaskingString {

        @DisplayName("Установлена одна группа масок. Одна маска для кирилицы")
        @ParameterizedTest(name = "Input [{0}] ExpectedOutput [{1}]")
        @CsvSource({
                "ИВАНОВ, АААААА",
                "ИВАНОВ иван, АААААА иван",
                "ABCDEFG, ABCDEFG",
                "ABCDEFG abc, ABCDEFG abc",
        })
        void maskForOneCharacterGroup(String input, String expected) {
            BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                    .addMaskForCharacters("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ", 'А')
                    .build();
            String actual = maskingAlgorithm.apply(input);
            assertThat(actual)
                    .isEqualTo(expected);
        }

        @DisplayName("Установлено множество групп масок. Кирилица, латиницы и цифры")
        @ParameterizedTest(name = "Input [{0}] ExpectedOutput [{1}]")
        @CsvSource({
                "ИВАНОВ, ББББББ",
                "Иванов, Бббббб",
                "Smith, Fffff",
                "SMITH, FFFFF",
                "888 999 111, 111 111 111",
                "ORA-01test Ошибка произошла, FFF-11ffff Бббббб ббббббббб",
                "Meta ('_'), Ffff ('_')",
        })
        void maskForMultipleCharacterGroup(String input, String expected) {
            BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                    .addMaskForCharacters("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ", 'Б')
                    .addMaskForCharacters("абвгдеёжзийклмнопрстуфхцчшщъыьэюя", 'б')
                    .addMaskForCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 'F')
                    .addMaskForCharacters("abcdefghijklmnopqrstuvwxyz", 'f')
                    .addMaskForCharacters("0123456789", '1')
                    .build();
            String actual = maskingAlgorithm.apply(input);
            assertThat(actual)
                    .isEqualTo(expected);
        }
    }

    @DisplayName("Конструирование объекта")
    @Nested
    class MaskingConstruction {

        @Nested
        @DisplayName("Успешное создание объекта")
        class Positive {

            @DisplayName("При указании маски для одной группы символов")
            @Test
            void maskForOneCharacterGroup() {
                BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                        .addMaskForCharacters("ABCDE", 'A')
                        .build();
                List<BasicMaskingAlgorithm.CharMasker> masks = maskingAlgorithm.getCharactersGroups();
                assertThat(masks)
                        .hasSize(1);
            }

            @DisplayName("При указании маски для одной группы символов, второй вариант метода")
            @Test
            void maskForOneCharacterGroupMethodOverride() {
                BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                        .addMaskForCharacters("Test")
                        .build();
                List<BasicMaskingAlgorithm.CharMasker> masks = maskingAlgorithm.getCharactersGroups();
                assertThat(masks)
                        .hasSize(1);
            }

            @DisplayName("При указании масок для двух групп символов")
            @Test
            void masksForTwoCharacterGroups() {
                BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                        .addMaskForCharacters("0123456789", '1')
                        .addMaskForCharacters("ABCDEFG", 'A')
                        .build();
                List<BasicMaskingAlgorithm.CharMasker> masks = maskingAlgorithm.getCharactersGroups();
                assertThat(masks)
                        .hasSize(2);
            }

            @DisplayName("При указании масок для множества групп символов")
            @Test
            void maskForMultipleCharacterGroups() {
                BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                        .addMaskForCharacters("0123456789", '1')
                        .addMaskForCharacters("ABCDEFG", 'A')
                        .addMaskForCharacters("abcdefg", 'a')
                        .addMaskForCharacters("абвгдеёж", 'б')
                        .addMaskForCharacters("АБВГДЕЁЖЗ", 'Б')
                        .build();
                List<BasicMaskingAlgorithm.CharMasker> masks = maskingAlgorithm.getCharactersGroups();
                assertThat(masks)
                        .hasSize(5);
            }
        }

        @Nested
        @DisplayName("Объект создать не удалось")
        class Negative {

            @DisplayName("Попытка создать маскирующий алгоритм, не предоставляя символы на которые нужно реагировать и маску для них")
            @Test
            void noCharacterGroupAndMaskPresented() {
                Executable expression = () -> BasicMaskingAlgorithm.newInstance().build();
                Assertions.assertThrows(IllegalStateException.class, expression, "Удалось сконструировать объект BasicMaskingAlgorithm, но этого не должно происходить");
            }

            @DisplayName("Попытка создать маскер, когда вместо группы символов представлена пустая строка")
            @Test
            void attemptToAddEmptyStringAsCharacterGroup() {
                Executable expression = () -> BasicMaskingAlgorithm.newInstance()
                        .addMaskForCharacters("", 'X')
                        .build();
                Assertions.assertThrows(IllegalArgumentException.class, expression, "Удалось сконструировать объект BasicMaskingAlgorithm, но этого не должно происходить");
            }

            @DisplayName("Попытка создать маскер, когда вместо группы символов представлена null")
            @Test
            void attemptToAddNullAsCharacterGroup() {
                Executable expression = () -> BasicMaskingAlgorithm.newInstance()
                        .addMaskForCharacters(null, 'X')
                        .build();
                Assertions.assertThrows(IllegalArgumentException.class, expression, "Удалось сконструировать объект BasicMaskingAlgorithm, но этого не должно происходить");
            }
        }
    }
}
