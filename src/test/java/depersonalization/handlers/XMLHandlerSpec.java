package depersonalization.handlers;

import depersonalization.handlers.text.masking.SimpleMaskingAlgorithm;
import depersonalization.handlers.text.node.TextNodeProcessor;
import depersonalization.producers.StructuredMessage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


class XMLHandlerSpec {

    private static final String INPUT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<root>\n" +
            "    <sensetive attr01= \"one\" attr02 = \"two\">Петров И.И.</sensetive>\n" +
            "    <debt>\n" +
            "        <amount>666</amount>\n" +
            "        <currency>USD</currency>\n" +
            "    </debt>\n" +
            "</root>";

    private static final String EXPECTED = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<root>\n" +
            "    <sensetive attr01= \"XXX\" attr02 = \"XXX\">XXXXXXXXXXX</sensetive>\n" +
            "    <debt>\n" +
            "        <amount>XXX</amount>\n" +
            "        <currency>XXX</currency>\n" +
            "    </debt>\n" +
            "</root>";

    @DisplayName("Все значения атрибутов и значение полей будут заменены согласно установленному правилу")
    @Test
    void test() {
        TextNodeProcessor nodeProcessor = TextNodeProcessor.newInstance()
                .addRule((name, value) -> true)
                .setMaskingAlgorithm(new SimpleMaskingAlgorithm('X'))
                .build();

        XMLHandler xmlHandler = new XMLHandler(nodeProcessor);
        Optional<String> actual = xmlHandler.dePerson(new StructuredMessage() {
            @Override
            public String getType() {
                return "XML";
            }

            @Override
            public String getContent() {
                return INPUT;
            }
        });

        Optional<String> expected = Optional.of(EXPECTED);
        assertEquals(expected, actual);
    }
}
