package depersonalization;

import depersonalization.handlers.StructuredMessageHandler;
import depersonalization.handlers.XMLHandler;
import depersonalization.handlers.text.masking.BasicMaskingAlgorithm;
import depersonalization.handlers.text.node.TextNodeProcessor;
import depersonalization.handlers.text.node.rules.BasicTextNodeRule;
import depersonalization.handlers.text.node.rules.TextNodeRule;
import depersonalization.handlers.text.node.rules.finders.PersonNameSequenceFinder;
import depersonalization.handlers.text.node.rules.finders.PhoneNumberSequenceFinder;
import depersonalization.handlers.text.node.rules.finders.TextSequenceFinder;
import depersonalization.producers.StructuredMessage;
import depersonalization.producers.StructuredMessageFactory;
import depersonalization.producers.XMLMessageFactory;
import depersonalization.producers.resources.ResourceDirectory;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static depersonalization.utils.IOUtils.fileFromResource;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Story {

    private static final String EXPECTED = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<root>\n" +
            "    <sensetive>Бббббб Б.Б.</sensetive>\n" +
            "    <debt>\n" +
            "        <amount>666</amount>\n" +
            "        <currency>USD</currency>\n" +
            "    </debt>\n" +
            "</root>\n";

    @Test
    void run() {

        List<TextSequenceFinder> finders = new ArrayList<>();
        finders.add(new PersonNameSequenceFinder());
        finders.add(new PhoneNumberSequenceFinder());

        TextNodeRule rule = new BasicTextNodeRule(finders);

        BasicMaskingAlgorithm maskingAlgorithm = BasicMaskingAlgorithm.newInstance()
                .addMaskForCharacters("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ", 'Б')
                .addMaskForCharacters("абвгдеёжзийклмнопрстуфхцчшщъыьэюя", 'б')
                .addMaskForCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 'F')
                .addMaskForCharacters("abcdefghijklmnopqrstuvwxyz", 'f')
                .addMaskForCharacters("0123456789", '1')
                .build();

        TextNodeProcessor textNodeProcessor = TextNodeProcessor.newInstance()
                .addRule(rule)
                .setMaskingAlgorithm(maskingAlgorithm)
                .build();

        StructuredMessageHandler xmlHandler = new XMLHandler(textNodeProcessor);

        DePerson depersonalizer = DePerson.newInstance()
                .registerHandler(xmlHandler)
                .build();

        File directoryWithXML = fileFromResource("dictionaries/xml");
        StructuredMessageFactory factory = XMLMessageFactory.newInstance()
                .setResource(new ResourceDirectory(directoryWithXML))
                .build();

        String actual = null;
        if (factory.hasMoreMessages()) {
            StructuredMessage message = factory.nextMessage();
            Optional<String> dePersoned = depersonalizer.dePerson(message);
            if (dePersoned.isPresent()) {
                actual = dePersoned.get();
                System.out.println(actual);
            } else {
                System.out.println("Не удалось");
            }
        } else {
            System.out.println("nope");
        }
        assertEquals(EXPECTED, actual);
    }
}
