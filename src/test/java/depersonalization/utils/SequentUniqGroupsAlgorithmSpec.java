package depersonalization.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class SequentUniqGroupsAlgorithmSpec {

    @DisplayName("Запрещено передавать null в качестве аргумента")
    @Test
    void nullIsNotAllowed() {
        Assertions.assertThrows(NullPointerException.class, () -> new SequentUniqGroupsAlgorithm<>(null).find(2));
    }

    @DisplayName("Отрицательные числа и ноль нельзя исползовать для определения колличества элементов в группе")
    @ParameterizedTest
    @ValueSource(ints = {-100, -2, -1, 0})
    void negativeNumbersAndZerosAreNotAllowed(int groupSize) {
        List<Integer> elements = getListOfSize(2);
        Assertions.assertThrows(IllegalArgumentException.class, () -> new SequentUniqGroupsAlgorithm<>(elements).find(groupSize));
    }

    @DisplayName("Если размер списка элементов меньше размера группы, то будет возвращен список из одной группы")
    @Test
    void elementCountLessThenGroupSize() {
        List<Integer> elements = getListOfSize(2);
        List<List<Integer>> groups = new SequentUniqGroupsAlgorithm<>(elements).find(2);
        Assertions.assertAll(
                () -> Assertions.assertEquals(1, groups.size()),
                () -> Assertions.assertEquals(Collections.singletonList(elements), groups)
        );
    }

    @DisplayName("Поиск уникальных групп размером 1")
    @ParameterizedTest(name = "Количество элементов в группе [{0}], количество найденных групп [{1}]")
    @CsvSource({
            "1, 1",
            "2, 2",
            "3, 3",
            "100, 100",
    })
    void findAllSequentialGroupsOfSizeOne(int elementsCount, int foundGroupsCount) {
        List<Integer> elements = getListOfSize(elementsCount);
        List<List<Integer>> groups = new SequentUniqGroupsAlgorithm<>(elements).find(1);
        Assertions.assertEquals(groups.size(), foundGroupsCount);
    }

    @DisplayName("Поиск уникальных групп размером 2")
    @ParameterizedTest(name = "Количество элементов в группе [{0}], количество найденных групп [{1}]")
    @CsvSource({
            "2, 1",
            "3, 2",
            "4, 3",
            "5, 4",
            "6, 5",
            "100, 99",
    })
    void findAllSequentialGroupsOfSizeTwo(int elementsCount, int foundGroupsCount) {
        List<Integer> elements = getListOfSize(elementsCount);
        List<List<Integer>> groups = new SequentUniqGroupsAlgorithm<>(elements).find(2);
        Assertions.assertEquals(groups.size(), foundGroupsCount);
    }

    @DisplayName("Поиск уникальных групп размером 3")
    @ParameterizedTest(name = "Количество элементов в группе [{0}], количество найденных групп [{1}]")
    @CsvSource({
            "1, 1",
            "2, 1",
            "3, 1",
            "4, 2",
            "5, 3",
            "6, 4",
            "7, 5",
            "100, 98",
    })
    void findAllSequentialGroupsOfSizeThree(int elementsCount, int foundGroupsCount) {
        List<Integer> elements = getListOfSize(elementsCount);
        List<List<Integer>> groups = new SequentUniqGroupsAlgorithm<>(elements).find(3);
        Assertions.assertEquals(groups.size(), foundGroupsCount);
    }

    private List<Integer> getListOfSize(int size) {
        List<Integer> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
        return list;
    }
}
