package depersonalization.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class TextTokenizerSpec {

    private static final String TEXT = "Начало  чьё-то Деревнин-Иванов М.А. и завершение.";
    private static TextTokenizer textTokenizer;

    @BeforeAll
    static void init() {
        textTokenizer = new TextTokenizer(TEXT);
    }

    @Test
    void tokenizerGetAllWordsFromText() {
        List<String> words = textTokenizer.getWords();
        Assertions.assertEquals(7, words.size());
        Assertions.assertAll(
                () -> Assertions.assertEquals("Начало", words.get(0)),
                () -> Assertions.assertEquals("чьё-то", words.get(1)),
                () -> Assertions.assertEquals("Деревнин-Иванов", words.get(2)),
                () -> Assertions.assertEquals("М.", words.get(3), "М."),
                () -> Assertions.assertEquals("А.", words.get(4), "А."),
                () -> Assertions.assertEquals("и", words.get(5), "и"),
                () -> Assertions.assertEquals("завершение.", words.get(6))
        );
    }

    @Test
    void amountOfSequentWordGroupsOfSizeOneEqualsWordsCount() {
        List<List<String>> groupsOfWords = textTokenizer.getSequentGroupsOfSize(1);
        Assertions.assertEquals(7, groupsOfWords.size());
    }

    @Test
    void sequentGroupsOfSizeTwo() {
        List<List<String>> groupsOfWords = textTokenizer.getSequentGroupsOfSize(2);
        Assertions.assertEquals(6, groupsOfWords.size());
        Assertions.assertAll(
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("Начало", "чьё-то"))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("Деревнин-Иванов", "М."))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("А.", "и"))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("чьё-то", "Деревнин-Иванов"))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("М.", "А."))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("и", "завершение.")))
        );
    }

    @Test
    void sequentGroupsOfSizeThree() {
        List<List<String>> groupsOfWords = textTokenizer.getSequentGroupsOfSize(3);
        Assertions.assertEquals(5, groupsOfWords.size());
        Assertions.assertAll(
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("Начало", "чьё-то", "Деревнин-Иванов"))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("М.", "А.", "и"))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("чьё-то", "Деревнин-Иванов", "М."))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("А.", "и", "завершение."))),
                () -> Assertions.assertTrue(groupsOfWords.contains(listOf("Деревнин-Иванов", "М.", "А.")))
        );
    }

    private List<String> listOf(String... strings) {
        return new ArrayList<>(Arrays.asList(strings));
    }

}
