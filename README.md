
Some rules about new code
1. All new code should be checked by SonarQube
    * if you dont have sonar
        * you should download sonar cube to use it https://www.sonarqube.org/downloads/
        * after installation you should start sonar from /bin folder
    
    * have sonar
        * sonar plugin already added to pom.xml
        * execute mvn sonar:sonar to run static code analysis